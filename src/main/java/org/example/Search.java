package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Search {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Por favor, especifique a palavra-chave como argumento.");
            return;
        }

        String keyword = args[0];

        URI uri;
        try {
            uri = Search.class.getResource("/data").toURI();
        } catch (URISyntaxException e) {
            System.out.println("Erro ao obter o URI do diretório de recursos: " + e.getMessage());
            return;
        }

        Path myPath;
        if (uri.getScheme().equals("jar")) {
            FileSystem fileSystem;
            try {
                fileSystem = FileSystems.newFileSystem(uri, new HashMap<>());
            } catch (IOException e) {
                System.out.println("Erro ao abrir o sistema de arquivos do JAR: " + e.getMessage());
                return;
            }
            myPath = fileSystem.getPath("/data");
        } else {
            myPath = Paths.get(uri);
        }

        Stream<Path> walk;
        try {
            walk = Files.walk(myPath, 1);
        } catch (IOException e) {
            System.out.println("Erro ao ler o diretório de recursos: " + e.getMessage());
            return;
        }

        for (Path path : walk.filter(Files::isRegularFile).collect(Collectors.toList())) {
            searchKeywordInFile(path, keyword);
        }
    }

    private static void searchKeywordInFile(Path file, String keyword) {
        try {
            String content = Files.readString(file);

            if (containsAllKeywords(content, keyword)) {
                System.out.println("A palavra-chave \"" + keyword + "\" foi encontrada no arquivo: " + file.getFileName());
            }
        } catch (IOException e) {
            System.out.println("Ocorreu um erro ao ler o arquivo \"" + file.getFileName() + "\": " + e.getMessage());
        }
    }

    private static boolean containsAllKeywords(String content, String keyword) {
        String[] keywords = keyword.toLowerCase().split(" ");

        for (String key : keywords) {
            if (!content.toLowerCase().contains(key)) {
                return false;
            }
        }

        return true;
    }
}
