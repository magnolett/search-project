package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

public class SearchTest {
    private final static ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final static PrintStream originalOut = System.out;

    @BeforeAll
    public static void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @BeforeEach
    public void cleanUpStreams() {
        outContent.reset();
    }

    @AfterAll
    public static void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testKeywordFoundInOneFile() {
        Search.main(new String[]{"keyword"});
        String output = outContent.toString().trim();
        assertTrue(output.contains("keyword"), "O output deveria conter 'keyword'");
        assertTrue(output.contains("file1.txt"), "O output deveria conter 'file1.txt'");
    }

    @Test
    public void testKeywordFoundInMultipleFiles() {
        Search.main(new String[]{"example"});
        String output = outContent.toString().trim();
        assertTrue(output.contains("example"), "O output deveria conter 'example'");
        assertTrue(output.contains("file1.txt"), "O output deveria conter 'file1.txt'");
        assertTrue(output.contains("file2.txt"), "O output deveria conter 'file2.txt'");
    }

    @Test
    public void testKeywordNotFound() {
        Search.main(new String[]{"random"});
        String output = outContent.toString().trim();
        assertFalse(output.contains(".txt"), "O output não deveria conter '.txt'");
    }
}
