# search-project



## Como rodar o projeto

Basta você clonar o repositório e, no terminal de sua preferência, ir até "seu_workspace/searchproject" e executar, neste terminal, o comando "./gradlew clean build".

Esse comando mostrará no seu console o processo de build e execução dos testes do projeto.

Após executá-lo, bastará ir à /builds/libs e então executar o jar com a keyword de sua preferência, exemplo:

* java -jar .\searchproject-1.0-SNAPSHOT.jar "walt disney"

Pronto! O output será exibido diretamente no console.
